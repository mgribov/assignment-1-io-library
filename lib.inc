section .data
numlib: db '0123456789'


;str: db '-12342'

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, 60
    mov rax, rdi ;в задании указано, что подпрограмма принимает код выхода => надо прочитать его 	из rdi
    xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .cycle:
    	cmp byte[rdi+rax], 0
    	je .return
    	inc rax
    	jmp .cycle
    .return:
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length 
    pop rdi
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ; это нужно, поскольку функция принимает КОД символа, а не непосредственно символ.
    mov rsi, rsp
    pop rdi
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rsp, `\n`
	call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r9, rdi ; записываем в регистр число
    mov rcx, 0 ; готовим счётчик
	push rcx
    .cycle:
    	mov rax, r9 
    	xor rdx, rdx ; чистим регистр для остатка 
    	mov r9, 10
    	div r9
		mov r8, [numlib + rdx]
		bswap r8
    	push r8
		add rsp, 7
    	inc rcx
    	cmp rax, 0
    	je .output
    	mov r9, rax ; частное записываем обратно в r9
    	jmp .cycle
    .output:
    	mov rdi, rsp
    	push rcx
    	call print_string
    	pop rcx
		add rsp, rcx - 1
		pop rdi
    	ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    mov rax, rdi
    shr rax, 63
    cmp rax, 0
    je .output
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .output:
    	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax
	.cycle: 
		mov r9b, [rdi + rax]
    		cmp byte[rsi + rax], r9b
    		jne .exit0
    		cmp byte[rsi + rax], 0
    		je .exit1
    		inc rax
    		jmp .cycle
    	.exit1:
    		mov rax, 1
    		ret
    	.exit0:
    		mov rax, 0
    		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    sub rsp, 8 ;нам надо выделить ячейку в памяти, куда будет записан символ из stdin. Так как я не очень хорошо понимаю, по какому адресу можно сохранять значения, пусть за меня это сделает стэк -_-. Для этого просто выделю себе ячейку там.
    mov rdx, 1
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    syscall
    cmp rax, 0
    je .end
    mov rax, [rsp]
    jmp .end
    .end:
    	add rsp, 8
    	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	;rdi - начало буфера
	;rsi - размер
	mov r8, rdi
	.cycle1:
		push r8
		push rdi
		push rsi
		call read_char
		pop rsi
		pop rdi
		pop r8
		cmp rax, ' '
		je .cycle1
		cmp rax, `\t`
		je .cycle1
		cmp rax, `\n`
		je .cycle1
		cmp rax, 0
		je .end
		mov byte[r8], al
		inc r8
	.cycle2:
		push r8
		push rdi
		sub r8, rdi
		cmp r8, rsi
		je .failure
		push rsi
		call read_char
		pop rsi
		pop rdi
		pop r8
		cmp rax, 0
		je .end
		cmp rax, ' '
		je .end
		cmp rax, '\t'
		je .end
		cmp rax, '\n'
		je .end
		mov byte[r8], al
		inc r8
		jmp .cycle2
	.end:
		mov byte[r8], 0
		sub r8, rdi
		mov rdx, r8
		mov rax, rdi
		ret
	.failure:
		pop rdi
		pop r8
		mov rax, 0 
    		ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor rsi, rsi
    xor r8, r8
    .cycle:
        mov sil, byte[rdi + rdx]
        cmp sil, '0' 
        jb .end
        cmp sil, '9'
        ja .end
        sub sil, '0' ; преобразуем строку в число
		imul rax, 10 ; умножаем предыдущий результат на 10
        add rax, rsi
        inc rdx
        jmp .cycle
    .end:
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rsi, rsi
    mov sil, byte[rdi]
    cmp sil, '-'
    jne .cycle2
    inc rdi
    call parse_uint
    cmp rdx, 0
    jbe .end
    neg rax
    inc rdx
    jmp .end
    .cycle2:
        call parse_uint
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r11, r11
    ;call string_length не получилось через уже написанную функцию. Странно. Так и не смог побороть ошибку Segmentation fault
    .cycle:
    	cmp r11, rdx
    	jae .failure
     	mov r9b, byte[rdi+r11]
     	mov byte[rsi+r11], r9b
     	cmp r9b, 0
     	je .end
     	inc r11
     	jmp .cycle
     .end:
    	mov rax, r11
    	ret
    .failure:
    	xor rax, rax
    	ret
     	
